@extends('layout.master')

@section('judul')
    Sign Up
@endsection

@section('content')
<form action="/welcome" method="post">
    @csrf
    <label for="fname">First name</label><br><br>
    <input type="text" id="fname" name="fname"><br><br>
    <label for="lname">Last name</label><br><br>
    <input type="text" id="lname" name="lname"><br><br>
    <label for="gender">Gender</label><br><br>
    <input type="radio" name="gender" id="male" value="Male">
    <label for="male">Male</label><br>
    <input type="radio" name="gender" id="female" value="Female">
    <label for="female" >Female</label><br><br>
    <label for="nationality">Nationality</label><br><br>
    <select name="nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="WNA">WNA</option>
    </select>
    <br><br>

    <label for="language">Language Spoken</label><br><br>
    <input type="checkbox" name ="language" id="1" value="Bahasa Indonesia">
    <label for="1">Indonesia</label><br>
    <input type="checkbox" name ="language" id="2" value="English">
    <label for="2">English</label><br>
    <input type="checkbox" name ="language" id="3" value="Other">
    <label for="2">Other</label><br><br>
    
    <label for="Bio">Bio</label><br><br>
    <textarea name="bio" rows="10" cols="30"></textarea>
    <br><br>
    <input type="submit" value="Sign Up">
</form>
@endsection