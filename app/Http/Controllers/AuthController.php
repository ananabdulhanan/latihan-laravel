<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(){
        return view('welcome');
    }

    public function submit(Request $request){
        $fname = $request['fname'];
        $lname = $request['lname'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $language = $request['language'];
        $bio = $request['bio'];
        return view('welcome', compact('fname','lname','gender','nationality','language','bio'));
    }
}